// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as help from './watcherhelp';

export function init(context: vscode.ExtensionContext) {
    const command = 'chelper.pageHelp';

    const commandHandler = () => {
        help.showHelpNotification();
    };

    context.subscriptions.push(vscode.commands.registerCommand(command, commandHandler));
}