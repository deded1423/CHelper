
import * as fs from 'fs';
import * as utils from '.././utils';
import * as path from 'path';
import * as vscode from 'vscode';
import * as cp from 'child_process';
import * as process from 'process';
import localize from ".././localize";
import * as gcc from "./html/gccHtml";
import * as valgrind from "./html/valgrindHtml";
import * as cppcheck from "./html/cppcheckHtml";
import * as flawfinder from "./html/flawfinderHtml";

const wsname = vscode.workspace.name;
var config = vscode.workspace.getConfiguration("chelper");

interface CheckerTaskDefinition extends vscode.TaskDefinition {
	/**
	 * Label of the task
	 */
	label: string;
	/**
	 * Gcc arguments used to compile the program
	 */
	gccargument: string;
	/**
	 * Tool to use to analyze
	 */
	tool: string;
	/**
	 * Tool arguments used to analyze the program
	 */
	toolargument: string;
}

export class CheckerTaskProvider implements vscode.TaskProvider {
	static checkerScriptType = 'chelper';
	private tasks: vscode.Task[] | undefined;
	private filename: string | undefined;

	// We use a CustomExecution task when state needs to be shared accross runs of the task or when 
	// the task requires use of some VS Code API to run.
	// If you don't need to share state between runs and if you don't need to execute VS Code API in your task, 
	// then a simple ShellExecution or ProcessExecution should be enough.
	// Since our build has this shared state, the CustomExecution is used below.
	private sharedState: string | undefined;

	constructor(private workspaceRoot: string) { }

	public async provideTasks(): Promise<vscode.Task[]> {
		return this.getTasks();
	}

	public resolveTask(_task: vscode.Task): vscode.Task | undefined {
		const label: string = _task.definition.label;
		const gccargument: string = _task.definition.gccargument;
		const tool: string = _task.definition.tool;
		const toolargument: string = _task.definition.toolargument;
		if (gccargument && tool && toolargument && label) {
			const definition: CheckerTaskDefinition = <any>_task.definition;
			return this.getTask(definition.label, definition.gccargument, definition.tool, definition.toolargument, definition);
		}
		return undefined;
	}

	private getTasks(): vscode.Task[] {
		if (this.tasks !== undefined && this.filename === vscode.window.activeTextEditor?.document.fileName) {
			return this.tasks;
		}
		if (wsname === undefined) {
			this.tasks = [];
			return this.tasks;
		}

		this.filename = vscode.window.activeTextEditor?.document.fileName;

		const tools: string[] = ['valgrind', 'flawfinder', 'cppcheck'];

		const toolarguments: string[] = [
			'valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --log-file=valgrind.out ./' + wsname,
			'flawfinder **.c **.h',
			'flawfinder $filename',
			'cppcheck **.c **.h',
			'cppcheck $filename'
		];

		this.tasks = [];

		this.tasks!.push(
			this.getTask(localize("checker.task.compileall.gcc"), '**.c **.h -g -Wall -o '.concat(wsname), 'gcc', './'+wsname));

		if (this.filename !== undefined) {
			if (this.filename.includes('.c')) {
				this.tasks!.push(this.getTask(localize("checker.task.compilefile.gcc").replace("$filename", this.filename),
				 this.filename.concat(' -g -Wall -o '.concat(wsname)), 'gcc', './'+wsname));
			}
		}
		this.tasks!.push(this.getTask(localize("checker.task.compileall.valgrind"), '**.c **.h -g -Wall -o '.concat(wsname), 'valgrind', toolarguments[0]));
		if (this.filename !== undefined) {
			if (this.filename.includes('.c')) {
				this.tasks!.push(this.getTask(localize("checker.task.compilefile.valgrind").replace("$filename", this.filename), this.filename.concat(' -g -Wall -o '.concat(wsname)), 'valgrind', toolarguments[0]));
			}
		}
		this.tasks!.push(this.getTask(localize("checker.task.compileall.flawfinder"), '-', 'flawfinder', toolarguments[1]));
		if (this.filename !== undefined) {
			if (this.filename.includes('.c')) {
				this.tasks!.push(this.getTask(localize("checker.task.compilefile.flawfinder").replace("$filename", this.filename), '', 'flawfinder', toolarguments[2].replace("$filename", this.filename)));
			}
		}
		this.tasks!.push(this.getTask(localize("checker.task.compileall.cppcheck"), '-', 'cppcheck', toolarguments[3]));
		if (this.filename !== undefined) {
			if (this.filename.includes('.c')) {
				this.tasks!.push(this.getTask(localize("checker.task.compilefile.cppcheck").replace("$filename", this.filename), '', 'cppcheck', toolarguments[4].replace("$filename", this.filename)));
			}
		}

		return this.tasks;
	}

	private getTask(label: string, gccargument: string, tool: string, toolargument: string, definition?: CheckerTaskDefinition): vscode.Task {
		if (definition === undefined) {
			definition = {
				type: CheckerTaskProvider.checkerScriptType,
				label,
				gccargument,
				tool,
				toolargument
			};
		}
		return new vscode.Task(definition, vscode.TaskScope.Workspace, label,
			CheckerTaskProvider.checkerScriptType, new vscode.CustomExecution(async (): Promise<vscode.Pseudoterminal> => {
				return new CheckerTaskTerminal(this.workspaceRoot, label, gccargument, tool, toolargument, 
					() => this.sharedState, (state: string) => this.sharedState = state);
			}));
	}
}

class CheckerTaskTerminal implements vscode.Pseudoterminal {
	private writeEmitter = new vscode.EventEmitter<string>();
	onDidWrite: vscode.Event<string> = this.writeEmitter.event;
	private closeEmitter = new vscode.EventEmitter<number>();
	onDidClose?: vscode.Event<number> = this.closeEmitter.event;

	private childProcess: cp.ChildProcess | undefined = undefined;

	constructor(private workspaceRoot: string, private label: string, private gccargument: string, private tool: string, private toolargument: string, private getSharedState: () => string | undefined, private setSharedState: (state: string) => void) {
	}

	open(initialDimensions: vscode.TerminalDimensions | undefined): void {
		// At this point we can start using the terminal.
		this.doBuild();
	}

	close(): void {
		// The terminal has been closed. Shutdown the build.
		this.childProcess?.kill("SIGKILL");
	}

	handleInput(data: string): void {
		console.log(data);
		this.writeEmitter.fire(data === '\r' ? '\r\n' : data);
		if (this.childProcess !== undefined && this.childProcess.stdin !== null) {
			this.childProcess.stdin.write(data);
		}
	}

	private async doBuild(): Promise<void> {
		return new Promise<void>((resolve) => {
			if (vscode.workspace.workspaceFolders === undefined) {
				this.writeEmitter.fire(localize("checker.noworkspace") + '\r\n');
				this.closeEmitter.fire(0);
				resolve();
				return;
			}

			var workspaceFolder = vscode.workspace.workspaceFolders[0].uri.fsPath;


			if ((this.tool !== 'valgrind' && this.tool !== 'gcc') || this.gccargument === '-') {
				//NO GCC
				this.analyzeTool(resolve, workspaceFolder);
				this.closeEmitter.fire(0);
				resolve();
				return;
			}


			this.writeEmitter.fire(localize("checker.compiling") + '\r\n');
			this.writeEmitter.fire(localize("checker.usinggcc") + ' ' + this.gccargument + '\r\n');

			let gccprocess: cp.ChildProcessWithoutNullStreams;
			if (this.label.includes("WINDOWSDEBUG")) {
				gccprocess = cp.spawn('dir', { cwd: workspaceFolder, env: process.env, shell: true });
			} else {
				gccprocess = cp.spawn('gcc ' + this.gccargument + " 2> gcc.out", { cwd: workspaceFolder, env: process.env, shell: true });
			}

			gccprocess.on('error', (err) => {
				this.writeEmitter.fire(localize("checker.compilererror") + '\r\n' + err + '\r\n');
				throw err;
			});

			this.childProcess = gccprocess;
			console.log(`PID is ${gccprocess.pid}`);

			gccprocess.on('exit', async (code, signal) => {
				await utils.sleep(100);
				if (code !== 0) {
					this.writeEmitter.fire(localize("checker.compilercode") + ' ' + code + '\r\n');
				} else {
					this.writeEmitter.fire(localize("checker.compilerok") + ' ' + code + '\r\n');
				}
				if (vscode.workspace.workspaceFolders === undefined) {
					this.closeEmitter.fire(0);
					resolve();
					return;
				}
				const onDiskPath = vscode.Uri.file(
					path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, 'gcc.out')
				);

				var gccoutput: string;
				vscode.workspace.openTextDocument(onDiskPath).then((document) => {
					if (document.getText() === '') {
						this.writeEmitter.fire(localize("checker.shownothing") + '\r\n');
					} else {
						gccoutput = document.getText();
						gcc.showPage(gccoutput, workspaceFolder);
					}
					if (config.get<boolean>("removeOutFiles", true)) {
						fs.unlinkSync(onDiskPath.fsPath);
					}
				});

				if (code === 0) {
					this.analyzeTool(resolve, workspaceFolder);
				} else {
					this.closeEmitter.fire(0);
					resolve();
					return;
				}
			});
		});
	}

	private analyzeTool(resolve: (value: void | PromiseLike<void>) => void, workspaceFolder: string) {
		this.writeEmitter.fire(localize("checker.analyzing")+' ' + this.toolargument + '\r\n');
		switch (this.tool) {
			case 'valgrind':
				this.useValgrind(resolve, workspaceFolder);
				break;
			case 'flawfinder':
				this.useFlawfinder(resolve, workspaceFolder);
				break;
			case 'cppcheck':
				this.useCppcheck(resolve, workspaceFolder);
				break;
			case 'gcc':
				this.useGcc(resolve, workspaceFolder);
				break;

			default:
				this.writeEmitter.fire(localize("checker.notool") + ' ' + this.tool);
				this.closeEmitter.fire(0);
				resolve();
		}
	}
	private useValgrind(resolve: (value: void | PromiseLike<void>) => void, workspaceFolder: string) {

		this.childProcess = cp.spawn('stdbuf -oL -eL ' + this.toolargument,
			{
				cwd: workspaceFolder,
				env: process.env,
				shell: true
			}
		);
		if (this.childProcess.stdout === null || this.childProcess.stderr === null) {
			this.writeEmitter.fire(localize("checker.valgrind.error") + ' \r\n');
			this.closeEmitter.fire(0);
			resolve();
			return;
		}

		this.childProcess.stdout.setEncoding('utf8');
		this.childProcess.stderr.setEncoding('utf8');

		this.childProcess.stdout.on('data', (data) => {
			console.log(data.toString());
			this.writeEmitter.fire(data.toString());
		});
		this.childProcess.stderr.on('data', (data) => {
			console.log('stderr: ' + data.toString());
			this.writeEmitter.fire(data.toString());
		});
		this.childProcess.on('error', (err) => {
			this.writeEmitter.fire(localize("checker.valgrind.error") + ' \r\n' + err + '\r\n');
			throw err;
		});
		this.childProcess.on('exit', async (code, signal) => {
			await utils.sleep(100);
			const onDiskPath = vscode.Uri.file(
				path.join(workspaceFolder, 'valgrind.out')
			);
			this.writeEmitter.fire(onDiskPath + '\r\n');
			var valgrindoutput: string;
			vscode.workspace.openTextDocument(onDiskPath).then((document) => {
				if (document.getText() === '') {
					this.writeEmitter.fire(localize("checker.shownothing") + '\r\n');
				} else {
					valgrindoutput = document.getText();
					valgrind.showPage(valgrindoutput, workspaceFolder);
				}
				if (config.get<boolean>("removeOutFiles", true)) {
					fs.unlinkSync(onDiskPath.fsPath);
				}
			});

			this.closeEmitter.fire(0);
			resolve();
		});
	}
	private useFlawfinder(resolve: (value: void | PromiseLike<void>) => void, workspaceFolder: string) {

		if (this.label.includes("WINDOWSDEBUG")) {
			this.childProcess = cp.spawn(this.toolargument,
				{
					cwd: workspaceFolder,
					env: process.env,
					shell: true
				}
			);
		}else{
			this.childProcess = cp.spawn(this.toolargument+ " > flawfinder.out",
				{
					cwd: workspaceFolder,
					env: process.env,
					shell: true
				}
			);
		}
		if (this.childProcess.stdout === null || this.childProcess.stderr === null) {
			this.writeEmitter.fire(localize("checker.flawfinder.error") + ' \r\n');
			this.closeEmitter.fire(0);
			resolve();
			return;
		}

		this.childProcess.stdout.setEncoding('utf8');
		this.childProcess.stderr.setEncoding('utf8');

		this.childProcess.stdout.on('data', (data) => {
			console.log(data.toString());
			this.writeEmitter.fire(data.toString());
		});
		this.childProcess.stderr.on('data', (data) => {
			console.log('stderr: ' + data.toString());
			this.writeEmitter.fire(data.toString());
		});
		this.childProcess.on('error', (err) => {
			this.writeEmitter.fire(localize("checker.flawfinder.error") + ' \r\n' + err + '\r\n');
			throw err;
		});
		this.childProcess.on('exit', async (code, signal) => {
			await utils.sleep(500);
			const onDiskPath = vscode.Uri.file(
				path.join(workspaceFolder, 'flawfinder.out')
			);
			this.writeEmitter.fire(onDiskPath + '\r\n');
			var flawfinderoutput: string;
			vscode.workspace.openTextDocument(onDiskPath).then((document) => {
				if (document.getText() === '') {
					this.writeEmitter.fire(localize("checker.shownothing") + '\r\n');
				} else {
					flawfinderoutput = document.getText();
					flawfinder.showPage(flawfinderoutput, workspaceFolder);
				}
				if (config.get<boolean>("removeOutFiles", true)) {
					fs.unlinkSync(onDiskPath.fsPath);
				}
			});

			this.closeEmitter.fire(0);
			resolve();
		});
	}
	private useCppcheck(resolve: (value: void | PromiseLike<void>) => void, workspaceFolder: string) {
		

		if (this.label.includes("WINDOWSDEBUG")) {
			this.childProcess = cp.spawn(this.toolargument,
				{
					cwd: workspaceFolder,
					env: process.env,
					shell: true
				}
			);
		}else{
			this.childProcess = cp.spawn(this.toolargument+ " &> cppcheck.out",
				{
					cwd: workspaceFolder,
					env: process.env,
					shell: true
				}
			);
		}

		if (this.childProcess.stdout === null || this.childProcess.stderr === null) {
			this.writeEmitter.fire(localize("checker.cppcheck.error") + ' \r\n');
			this.closeEmitter.fire(0);
			resolve();
			return;
		}

		this.childProcess.stdout.setEncoding('utf8');
		this.childProcess.stderr.setEncoding('utf8');

		this.childProcess.stdout.on('data', (data) => {
			console.log(data.toString());
			this.writeEmitter.fire(data.toString());
		});
		this.childProcess.stderr.on('data', (data) => {
			console.log('stderr: ' + data.toString());
			this.writeEmitter.fire(data.toString());
		});
		this.childProcess.on('error', (err) => {
			this.writeEmitter.fire(localize("checker.cppcheck.error") + ' \r\n' + err + '\r\n');
			throw err;
		});
		this.childProcess.on('exit', async (code, signal) => {
			await utils.sleep(500);
			const onDiskPath = vscode.Uri.file(
				path.join(workspaceFolder, 'cppcheck.out')
			);
			this.writeEmitter.fire(onDiskPath + '\r\n');
			var cppcheckoutput: string;
			vscode.workspace.openTextDocument(onDiskPath).then((document) => {
				if (document.getText() === '') {
					this.writeEmitter.fire(localize("checker.shownothing") + '\r\n');
				} else {
					cppcheckoutput = document.getText();
					cppcheck.showPage(cppcheckoutput, workspaceFolder);
				}
				if (config.get<boolean>("removeOutFiles", true)) {
					fs.unlinkSync(onDiskPath.fsPath);
				}
			});

			this.closeEmitter.fire(0);
			resolve();
		});
	}
	private useGcc(resolve: (value: void | PromiseLike<void>) => void, workspaceFolder: string) {
		
		this.childProcess = cp.spawn('stdbuf -oL -eL ' + this.toolargument,
			{
				cwd: workspaceFolder,
				env: process.env,
				shell: true
			}
		);
		if (this.childProcess.stdout === null || this.childProcess.stderr === null) {
			this.writeEmitter.fire(localize("checker.gcc.error") + ' \r\n');
			this.closeEmitter.fire(0);
			resolve();
			return;
		}

		this.childProcess.stdout.setEncoding('utf8');
		this.childProcess.stderr.setEncoding('utf8');

		this.childProcess.stdout.on('data', (data) => {
			console.log(data.toString());
			this.writeEmitter.fire(data.toString());
		});
		this.childProcess.stderr.on('data', (data) => {
			console.log('stderr: ' + data.toString());
			this.writeEmitter.fire(data.toString());
		});
		this.childProcess.on('error', (err) => {
			this.writeEmitter.fire(localize("checker.gcc.error") + ' \r\n' + err + '\r\n');
			throw err;
		});
		this.childProcess.on('exit', async (code, signal) => {
			this.writeEmitter.fire(localize("checker.gcc.exit") + ' \r\n');
			this.closeEmitter.fire(0);
			resolve();
		});
	}
}


