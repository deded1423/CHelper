
import * as vscode from 'vscode';
import * as parser from 'fast-xml-parser';
import { CheckerTaskProvider } from './checkerTaskProvider';
import * as path from 'path';
import * as fs from 'fs';

export function initTasks(context: vscode.ExtensionContext) {
    console.log("Initialize Tasks DebugChecker");


    const workspaceRoot = (vscode.workspace.workspaceFolders && (vscode.workspace.workspaceFolders.length > 0))
        ? vscode.workspace.workspaceFolders[0].uri.fsPath : undefined;

    if (!workspaceRoot) {
        return;
    }
        
    var defaultcppfile = "errorcppcheck-en.xml";
    var cppfile = "errorcppcheck-"+JSON.parse(process.env.VSCODE_NLS_CONFIG || "{}").locale+".xml";
    var onDiskPath = vscode.Uri.file(
        path.join(vscode.extensions.getExtension("deded1423.chelper")?.extensionPath ?? "", 'src', 'task','xml', cppfile)
    );

    if (!fs.existsSync(onDiskPath.fsPath)) {
        console.log("Default errorcppcheck xml");
        onDiskPath = vscode.Uri.file(
            path.join(vscode.extensions.getExtension("deded1423.chelper")?.extensionPath ?? "", 'src', 'task','xml', defaultcppfile)
            ); 
    }

    vscode.workspace.openTextDocument(onDiskPath).then((document) => {

        var options = {
            attributeNamePrefix : "",
            textNodeName : "",
            ignoreAttributes : false,
            ignoreNameSpace : false,
            allowBooleanAttributes : false,
            parseNodeValue : true,
            parseAttributeValue : false,
        };

        var cpp = require('./html/cppcheckHtml');
        cpp.errorsJson = parser.parse(document.getText(),options).errors.error;
	});
        
    var defaultvalgrindfile = "errorvalgrind-en.xml";
    var valgrindfile = "errorvalgrind-"+JSON.parse(process.env.VSCODE_NLS_CONFIG || "{}").locale+".xml";
    var onDiskPath = vscode.Uri.file(
        path.join(vscode.extensions.getExtension("deded1423.chelper")?.extensionPath ?? "", 'src', 'task','xml', valgrindfile)
    );

    if (!fs.existsSync(onDiskPath.fsPath)) {
        console.log("Default errorvalgrind xml");
        onDiskPath = vscode.Uri.file(
            path.join(vscode.extensions.getExtension("deded1423.chelper")?.extensionPath ?? "", 'src', 'task','xml', defaultvalgrindfile)
            ); 
    }

    vscode.workspace.openTextDocument(onDiskPath).then((document) => {

        var options = {
            attributeNamePrefix : "",
            textNodeName : "",
            ignoreAttributes : false,
            ignoreNameSpace : false,
            allowBooleanAttributes : false,
            parseNodeValue : true,
            parseAttributeValue : false,
        };

        var valgrind = require('./html/valgrindHtml');
        valgrind.errorsJson = parser.parse(document.getText(),options).errors.error;
	});
                
    var defaultgccfile = "errorgcc-en.xml";
    var gccfile = "errorgcc-"+JSON.parse(process.env.VSCODE_NLS_CONFIG || "{}").locale+".xml";
    var onDiskPath = vscode.Uri.file(
        path.join(vscode.extensions.getExtension("deded1423.chelper")?.extensionPath ?? "", 'src', 'task','xml', gccfile)
    );

    if (!fs.existsSync(onDiskPath.fsPath)) {
        console.log("Default errorgcc xml");
        onDiskPath = vscode.Uri.file(
            path.join(vscode.extensions.getExtension("deded1423.chelper")?.extensionPath ?? "", 'src', 'task','xml', defaultgccfile)
            ); 
    }

    vscode.workspace.openTextDocument(onDiskPath).then((document) => {

        var options = {
            attributeNamePrefix : "",
            textNodeName : "",
            ignoreAttributes : false,
            ignoreNameSpace : false,
            allowBooleanAttributes : false,
            parseNodeValue : true,
            parseAttributeValue : false,
        };

        var gcc = require('./html/gccHtml');
        gcc.errorsJson = parser.parse(document.getText(),options).errors.error;
	});

    var checkerTaskProvider = vscode.tasks.registerTaskProvider(CheckerTaskProvider.checkerScriptType, new CheckerTaskProvider(workspaceRoot));
}