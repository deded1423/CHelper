
import * as utils from '../.././utils';
import * as vscode from 'vscode';
import localize from "../.././localize";

export async function  showPage(output: string, workspaceFolder: string) {
	var now = new Date();
	const panel = vscode.window.createWebviewPanel(
		'flawfinderPage', // Identifies the type of the webview. Used internally
		localize('flawfinder.webview.title')+` - ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`, // Title of the panel displayed to the user
		vscode.ViewColumn.One, // Editor column to show the new webview panel in.
		{ enableScripts: true, } // Webview options. More on these later.
	);

	output = output.replace(/\<(.*)\>/gm,'&lt;$1&gt;');

	output = '<p>' + output.replace(/\n/gi, '</p><p>')
	.replace(/(0x[\dABCDEF]+)/gi, '<span class="ui text olive">$1</span>')
	.replace(/FINAL RESULTS:/gi, '<br><span class="ui text red bold">FINAL RESULTS:</span>')
	.replace(/ANALYSIS SUMMARY:/gi, '<br><span class="ui text red bold">ANALYSIS SUMMARY:</span>')
	.replace(/([^\w])(int)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(char)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(struct)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(size_t)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(null)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(unsigned)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(short)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(long)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(float)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(long)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(double)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(long)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	.replace(/([^\w])(scanf)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
	;

	const regexp1 = /([\w\/\-\_)]*\.[ch])(?![:o])/gi;
	const regexp2 = /([\w\/\-\_]*\.[ch]):(\d+):/gi;
	const regexp3 = /([\w\/\-\_]*\.[ch]):(\d+):(\d+)/gi;

	var match;
	var matches: string[] = [];

	do {
		match = regexp1.exec(output);
		if (match && !matches.includes(match[1])) {
			matches.push(match[1]);
		}
	} while (match);

	for await (const match of matches){
		vscode.workspace.findFiles(match).then((doc) => {
			if (doc) {
				output = output.replaceAll(
					new RegExp(`\(${match}\)\(?!:\)`, "gi"),
					'<a onclick="openVSCode(\'$1\',0,0);">$1</a>');
			}
		});
	}

	var matches: string[] = [];
	do {
		match = regexp2.exec(output);
		if (match && !matches.includes(match[1])) {
			matches.push(match[1]);
		}
	} while (match);

	for await (const match of matches){
		var doc = await vscode.workspace.findFiles(match);
		if (doc.length !== 0) {
			var re = `(${match}:(\\d+)):`;
			var reg = new RegExp(re.replace(".", "\\."), "g");

			var match2;
			do {
				match2 = reg.exec(output);
				if (match2) {
					output = output.replace(match2[0],
						`<br><a onclick="openVSCode(\'${doc[0].path}\',${match2[2]},0);">${match2[1]}</a>:`);
				}
			} while (match2);
		}else{
			output = output.replaceAll(match,
				`<br><span class="ui info text">${match}</span>`);
		}
	}

	panel.webview.html = `
	<!DOCTYPE html>
	<html>
		<body style="background:transparent; color:var(--vscode-editor-foreground)">
			<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
			<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css">
			<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>

			<script>
				const vscode = acquireVsCodeApi();	
				function openVSCode(filename, line, column) {	
					console.log('openVSCode');			
					vscode.postMessage({
						file: filename,
						line: line,
						column: column
					});
				}
			</script>

			<style>
				p { margin:0 0 0.1em; }
				a:hover { cursor:pointer; }
				.bold { font-weight: bolder; }
			</style>

			<span>
				<pre class="ui labels" style="padding-left:10px; margin:0;">

				<p>${output}</p>

				</pre>
			</span>
		</body>
	</html>`;

	panel.webview.onDidReceiveMessage(
		message => {
			console.log(message.file + "  " + message.line + "  " + message.column);
			utils.openDocument(workspaceFolder, message.file, message.line, message.column);
		},
		undefined
	);
}