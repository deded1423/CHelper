
import * as utils from '../.././utils';
import * as vscode from 'vscode';
import localize from "../.././localize";

export var errorsJson:any;

export async function  showPage(this: any, output: string, workspaceFolder: string) {
	var now = new Date();
	const panel = vscode.window.createWebviewPanel(
		'valgrindPage', // Identifies the type of the webview. Used internally
		localize('valgrind.webview.title')+` - ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`, // Title of the panel displayed to the user
		vscode.ViewColumn.One, // Editor column to show the new webview panel in.
		{ enableScripts: true, } // Webview options. More on these later.
	);

	output = output.replace(/\n\-\-(\d)*\-\-.*/gm,'');

	output = '<p>' + output.replace(/\n/gi, '</p><p>')
	.replace(/(0x[\dABCDEF]+)/gi, '<span class="ui text olive">$1</span>')
	.replace(/LEAK SUMMARY:/gi, '<span class="ui text red bold">LEAK SUMMARY:</span>')
	.replace(/ERROR SUMMARY:/gi, '<span class="ui text red bold">ERROR SUMMARY:</span>')
	.replace(/HEAP SUMMARY:/gi, '<span class="ui text red bold">HEAP SUMMARY:</span>')
	.replace(/malloc/gi, '<span class="ui text purple bold">malloc</span>')
	.replace(/calloc/gi, '<span class="ui text purple bold">calloc</span>')
	.replace(/frees/gi, '<span class="ui text purple bold">frees</span>')
	.replace(/free(?=[^[sd]])/gi, '<span class="ui text purple bold">free</span>')
	.replace(/bytes/gi, '<span class="ui text purple bold">bytes</span>')
	.replace(/blocks/gi, '<span class="ui text purple bold">blocks</span>')
	.replace(/allocs/gi, '<span class="ui text purple bold">allocs</span>')
	;


	const regexp1 = /\(([\w\/\-\_)]*\.[ch])\)/gi;
	const regexp2 = /\(([\w\/\-\_]*\.[ch]):(\d+)\)/gi;
	const regexp3 = /\(([\w\/\-\_]*\.[ch]):(\d+):(\d+)\)/gi;

	var match;
	var matches: string[] = [];

	do {
		match = regexp1.exec(output);
		if (match && !matches.includes(match[1])) {
			matches.push(match[1]);
		}
	} while (match);

	matches.forEach(match => {
		console.log(match);
		vscode.workspace.findFiles(match).then((doc) => {
			if (doc) {
				output = output.replace(
					new RegExp(`\(${match}\)`, "gi"),
					'(<a onclick="openVSCode(\'$1\',0,0);">$1</a>)');
			}
		});
	});

	var matches: string[] = [];
	do {
		match = regexp2.exec(output);
		if (match && !matches.includes(match[1])) {
			matches.push(match[1]);
		}
	} while (match);

	for await (const match of matches){
		var doc = await vscode.workspace.findFiles(match);
		if (doc.length !== 0) {
			var re = `\\((${match}:(\\d+))\\)`;
			var reg = new RegExp(re.replace(".", "\\."), "g");

			var match2;
			do {
				match2 = reg.exec(output);
				if (match2) {
					output = output.replace(match2[0],
						`(<a onclick="openVSCode(\'${doc[0].path}\',${match2[2]},0);">${match2[1]}</a>)`);
				}
			} while (match2);
		}else{
			output = output.replaceAll(match,
				`<span class="ui info text">${match}</span>`);
		}
	}

	var matches: string[] = [];
	do {
		match = regexp3.exec(output);
		if (match && !matches.includes(match[1])) {
			matches.push(match[1]);
		}
	} while (match);


	for await (const err of this.errorsJson){
		var id = err.id;
		var msg = err.msg;
		if (id !== "") {
			output = output.replaceAll(`${id}`,
				`<span class ="onHoverShow ui green text" title="${msg}">` + `${id}` +
				`</span>`);
		}
	}

	panel.webview.html = `
	<!DOCTYPE html>
	<html>
		<body style="background:transparent; color:var(--vscode-editor-foreground)">
			<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
			<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css">
			<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>

			<script>
				const vscode = acquireVsCodeApi();	
				function openVSCode(filename, line, column) {	
					console.log('openVSCode');			
					vscode.postMessage({
						file: filename,
						line: line,
						column: column
					});
				}
			</script>

			<style>
				p { margin:0 0 0.1em; }
				a:hover { cursor:pointer; }
				.bold { font-weight: bolder; }
			</style>

			<span>
				<pre class="ui labels" style="padding-left:10px; margin:0;">

				<p>${output}</p>

				</pre>
			</span>
		</body>
	</html>`;

	panel.webview.onDidReceiveMessage(
		message => {
			console.log(message.file + "  " + message.line + "  " + message.column);
			utils.openDocument(workspaceFolder, message.file, message.line, message.column);
		},
		undefined
	);
}