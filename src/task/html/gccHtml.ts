
import * as utils from '../.././utils';
import * as vscode from 'vscode';
import localize from "../.././localize";

export var errorsJson:any;

export async function showPage(this: any, output: string, workspaceFolder: string) {
	var now = new Date();
	const panel = vscode.window.createWebviewPanel(
		'gccPage', // Identifies the type of the webview. Used internally
		localize('gcc.webview.title')+` - ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`, // Title of the panel displayed to the user
		vscode.ViewColumn.One, // Editor column to show the new webview panel in.
		{ enableScripts: true } // Webview options. More on these later.
	);

	output = output.replace(/\n/gi, '</p><p>')
		.replace(/fatal error:/gi, '<span class="ui text red">Fatal Error:</span>')
		.replace(/error:/gi, '<span class="ui text red">Error:</span>')
		.replace(/warning:/gi, '<span class="ui text yellow">Warning:</span>')
		.replace(/note:/gi, '<span class="ui text olive">Note:</span>')
		.replace(/ function /gi, ' <span class="ui text purple">function</span> ')
		.replace(/([^\w])(int)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(char)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(struct)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(size_t)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(null)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(unsigned)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(short)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(long)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(float)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(long)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(double)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/([^\w])(long)([^\w])/gi, '$1<span class="ui info text">$2</span>$3')
		.replace(/(([\w\/]*)\.[ch])(?=\s)/gi, '<a onclick="openVSCode(\'$1\',0,0);">$1</a> ')
		.replace(/(([\w\/]*)\.[ch])(?=:[^\d])/gi, '<a onclick="openVSCode(\'$1\',0,0);">$1</a>')
		.replace(/([\"\'\[])(([\w\/]*)\.[ch])([\"\'\]])[^\d+,\d+\);]/gi, '$1<a onclick="openVSCode(\'$2\',0,0);">$2</a>$4')
		.replace(/(([\w\/]*)\.[ch]):(\d+)(?=,[^\d])/gi, '<a onclick="openVSCode(\'$1\', $3,0);">$1:$3</a>')
		.replace(/(([\w\/]*)\.[ch]):(\d+)(?=:[^\d])/gi, '<a onclick="openVSCode(\'$1\', $3,0);">$1:$3</a>')
		.replace(/(([\w\/]*)\.[ch]):(\d+):(\d+)/gi, '<a onclick="openVSCode(\'$1\', $3, $4);">$1:$3:$4</a>');

	const regexp = /\[-W([\w\d\_\-]*)\]/g;
	var match;
	var matches: string[] = [];

	do {
		match = regexp.exec(output);
		if (match && !matches.includes(match[1])) {
			matches.push(match[1]);
		}
	} while (match);

	// matches.forEach(match => {
	// 	let exp = utils.getLocalizationWarning(match);
	// 	if (exp !== undefined) {
	// 		output = output.replaceAll(`\[-W${match}\]`,
	// 			`<span class ="onHoverShow ui green text" title="${exp}">` + `\[-W${match}\]` +
	// 			`</span>`);
	// 	} else {
	// 		console.log(localize("checker.nolocale") + match + ' ' + utils.getLocalizationWarning(match));
	// 	}
	// });

	for await (const err of this.errorsJson){
		var id = err.id;
		var msg = err.msg;
		if (id !== "") {
			output = output.replaceAll(`\[-W${id}\]`,
				`<span class ="onHoverShow ui green text" title="${msg}">` + `\[-W${id}\]` +
				`</span>`);
		}
	}

	
	panel.webview.html = `
	<!DOCTYPE html>
	<html>
		<body style="background:transparent; color:var(--vscode-editor-foreground)">
			<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
			<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css">
			<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>

			<script>
				const vscode = acquireVsCodeApi();	
				function openVSCode(filename, line, column) {	
					console.log('openVSCode');			
					vscode.postMessage({
						file: filename,
						line: line,
						column: column
					});
				}
			</script>

			<style>

				p { 
					margin:0 0 0.1em 
				}

				.hide {
					display: none !important;
					position:relative !important;
				}
					  
				.onHoverShow:hover + .hide {
				display: inline !important;
				}

				a:hover { cursor:pointer; }
			</style>

			<span>
				<pre class="ui labels" style="padding-left:10px; margin:0;">

				<p>${output}</p>

				</pre>
			</span>
		</body>
	</html>`;
	
	panel.webview.onDidReceiveMessage(
		message => {
			console.log(message);
			console.log(message.file + "  " + message.line + "  " + message.column);
			utils.openDocument(workspaceFolder, message.file, message.line, message.column);
		},
		undefined
	);
}