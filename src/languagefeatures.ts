
import * as vscode from 'vscode';
import localize from "./localize";
import * as help from './watcherhelp';
import * as checker from './task/debugchecker';
import * as commands from './commands';

export function init(context: vscode.ExtensionContext) {

    let c: vscode.DocumentFormattingEditProvider = {
        provideDocumentFormattingEdits(doc, pos, token): vscode.ProviderResult<vscode.TextEdit[]> {
            var changes: vscode.TextEdit[] = [];
            var offset = 0;
            for (let i = 0; i < doc.lineCount; i++) {
                const line = doc.lineAt(i);

                //Enter after func
                if (line.text.includes("}") && line.text.replaceAll('\t', "").trim() !== "}" && line.text.replaceAll('\t', "").replaceAll(' ', "").search(/\S+\}/g)!==-1) {
                    var ins = line.text.search("}");
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins), "\n"));
                    if (offset * pos.tabSize > 0) {
                        changes.push(vscode.TextEdit.insert(line.range.start, " ".repeat((offset-1) * pos.tabSize)));
                    }
                }
                if (line.text.includes("}") && line.text.replaceAll('\t', "").trim() !== "}" && line.text.replaceAll('\t', "").replaceAll(' ', "").search(/\}\S+/g)!==-1) {
                    var ins = line.text.search("}");
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins+1), "\n"));
                    if (offset * pos.tabSize > 0) {
                        changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins), " ".repeat((offset-1) * pos.tabSize)));
                    }
                }
                if (line.text.includes("{") && line.text.replaceAll('\t', "").trim() !== "{" && line.text.replaceAll('\t', "").replaceAll(' ', "").search(/\S+\{/g)!==-1) {
                    console.log("1 "+line.text);
                    var ins = line.text.search("{");
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins), "\n"));
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins), " ".repeat((offset) * pos.tabSize)));
                }
                if (line.text.includes("{") && line.text.replaceAll('\t', "").trim() !== "{" && line.text.replaceAll('\t', "").replaceAll(' ', "").search(/\{\S+/g) !==-1) {
                    console.log("2 "+line.text);
                    var ins = line.text.search("{");
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins+1), "\n"));
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins), " ".repeat((offset) * pos.tabSize)));
                }

                //Tabulados
                if (line.text.includes("}")) {
                    offset -= line.text.replace(/[^\}]/g, "").length;
                    if (line.text.includes("{")) {
                        offset += line.text.replace(/[^\{]/g, "").length;
                    }
                }

                var dif = offset * pos.tabSize - line.firstNonWhitespaceCharacterIndex;
                if (dif !== 0) {
                    if (dif > 0) {
                        changes.push(vscode.TextEdit.insert(line.range.start, " ".repeat(dif)));
                    } else {
                        changes.push(vscode.TextEdit.delete(new vscode.Range(line.range.start, line.range.start.translate(0, -dif))));
                    }
                }
                if (line.text.includes("{") && !line.text.includes("}")) {
                    offset += line.text.replace(/[^\{]/g, "").length;
                }
                if (offset < 0) {
                    offset = 0;
                }

                //Igual y mayor, etc
                if (line.text.search(/([^\s=><])[=<]/g) !== -1) {
                    var ins = line.text.search(/([^\s=><])[=<]/g);
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins + 1), " "));
                }
                if (line.text.search(/[=<]([^\s=><])/g) !== -1) {
                    var ins = line.text.search(/[=<]([^\s=><])/g);
                    console.log(ins);
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins + 1), " "));
                }

                if (line.text.search(/([^\s=><\-])>/g) !== -1) {
                    var ins = line.text.search(/([^\s=><\-])>/g);
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins + 1), " "));
                }
                if (line.text.search(/([^\-])>([^\s=><])/g) !== -1) {
                    var ins = line.text.search(/([^\-])>([^\s=><])/g);
                    changes.push(vscode.TextEdit.insert(line.range.start.translate(0, ins + 1), " "));
                }

            }
            return changes;
        }
    };

    vscode.languages.registerDocumentFormattingEditProvider('c', c);
}