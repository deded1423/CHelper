
import * as cp from 'child_process';
import localize from "./localize";
import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';


export function delay(ms: number) {
    var timeoutVar : any;
    var promise = new Promise(resolve => { timeoutVar = setTimeout(resolve, ms); });
    return {
        func: promise,
        timeout: timeoutVar
    };
}
export function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
export function exec(command: string, options: cp.ExecOptions): Promise<{ stdout: string; stderr: string }> {
    return new Promise<{ stdout: string; stderr: string }>((resolve, reject) => {
        cp.exec(command, options, (error, stdout, stderr) => {
            if (error) {
                reject({ error, stdout, stderr });
            }
            resolve({ stdout, stderr });
        });
    });
}
export function getLocalizationWarning(warning: string): string | undefined {
    let s = localize("warning."+warning);
    if ("warning."+warning === s) {
        return undefined;
    }
    return localize("warning."+warning);
}

export function openDocument(workspaceFolder: string, filename: string, line: number, column: number) {

	var onDiskPath = vscode.Uri.file(filename);


	if (!fs.existsSync(onDiskPath.fsPath)) {
		onDiskPath = vscode.Uri.file(
			path.join(workspaceFolder, filename)
		); 	
	}



	vscode.workspace.openTextDocument(onDiskPath).then((document) => {
		if (line === 0) {
			line = 1;
		}
		if (column === 0) {
			column = 1;
		}
		const edit = vscode.window.showTextDocument(document, { selection: new vscode.Range(line - 1, column - 1, line - 1, column - 1) });
	});
}