
import * as fs from 'fs';
import localize from "./localize";
import * as vscode from 'vscode';
import * as utils from './utils';
import * as path from 'path';

var config = vscode.workspace.getConfiguration("chelper");
var helpTimeout : NodeJS.Timeout;
var neverShowHelp = false;

export function init(context: vscode.ExtensionContext) {
    console.log("Initialize Help");

    // TIMEOUT PAG DE APOYO

    vscode.window.onDidChangeTextEditorSelection(()=>{
        if (vscode.window.activeTextEditor === undefined) {
            return;
        }
        setTimerHelp();
    });
    vscode.window.onDidChangeActiveTextEditor(async ()=>{
        if (vscode.window.activeTextEditor === undefined) {
            return;
        }
        setTimerHelp();
    });
}
function setTimerHelp() {
    if (!config.get<boolean>("showHelp",false) || neverShowHelp){
        return;
    }
    var delay = utils.delay(config.get<number>("timeHelpDelay",10)*1000);
    delay.func.then(() => showHelpNotification());
    if (helpTimeout !== null) {
        clearTimeout(helpTimeout);
    }
    helpTimeout = delay.timeout;
}
export function showHelpNotification() {
    console.log("Showing help after: " + config.get<number>("timeHelpDelay",10)*1000 + "ms");
    vscode.window.showInformationMessage(localize('help.text'), localize('help.yes'), localize('help.no'), localize('help.never'))
    .then(selection => {
        switch (selection) {
            case localize('help.yes'):
                const panel = vscode.window.createWebviewPanel(
                    'helpPage', // Identifies the type of the webview. Used internally
                    localize('help.webview.title'), // Title of the panel displayed to the user
                    vscode.ViewColumn.One, // Editor column to show the new webview panel in.
                    {} // Webview options. More on these later.
                );
                var defaulthtmlfile = "help-en.html";
                var htmlfile = "help-"+JSON.parse(process.env.VSCODE_NLS_CONFIG || "{}").locale+".html";
                var onDiskPath = vscode.Uri.file(
                path.join(vscode.extensions.getExtension("deded1423.chelper")?.extensionPath ?? "", 'src', 'webview', htmlfile)
                );

                if (!fs.existsSync(onDiskPath.fsPath)) {
                    console.log("Default help page");
                    onDiskPath = vscode.Uri.file(
                        path.join(vscode.extensions.getExtension("deded1423.chelper")?.extensionPath ?? "", 'src', 'webview', defaulthtmlfile)
                        ); 
                }

                vscode.workspace.openTextDocument(onDiskPath).then((document) => {
                  panel.webview.html = document.getText();
                });
                break;
        
            case localize('help.no'):            
                break;
    
            case localize('help.never'):
                config.update("showHelp",false);
                neverShowHelp = true;
                break;
                    
            default:
                break;
        }
      });
}
