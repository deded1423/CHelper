// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import localize from "./localize";
import * as help from './watcherhelp';
import * as checker from'./task/debugchecker';
import * as commands from './commands';
import * as language from './languagefeatures';

let diagnosticCollection: vscode.DiagnosticCollection;
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

export function activate(context: vscode.ExtensionContext) {
	
	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log(localize('loadedExtension.text'));

	help.init(context);
	checker.initTasks(context);
	commands.init(context);
	language.init(context);
}

// this method is called when your extension is deactivated
export function deactivate() {}
