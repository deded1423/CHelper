# CHelper README

It's a extension that aims to help when programming with C in Linux.
It adds several snippets, a task provider to speed the compiling and analysis of the errors with valgrind, cppcheck and flawfinder.

## Features

Adds snippets of C language to speed up productivity.

Adds a task provider to help when compiling and using valgrind, cppcheck and flawfinder.

Shows the output of valgrind, cppcheck and flawfinder formatted and with some explanation of some errors/warnings.

## Requirements


Requires gcc, valgrind, cppcheck, flawfinder and stdbuf.

Requires Linux

## Extension Settings


This extension contributes the following settings:

* `chelper.showHelp`: Check to show help if not writing for a period of time
* `chelper.timeHelpDelay`: Seconds to wait to show help
* `chelper.removeOutFiles`: Remove .out files after compiling or analyzing
## Known Issues

No known issues.

Please report issues to https://gitlab.com/deded1423/CHelper/issues

## Release Notes

### 1.0.0

Initial release of Chelper
